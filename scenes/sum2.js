sum3Button = new Button("Next",w/2-90,330,180,40,"lightgray","white","black","30px Arial")

_buttonsArr.splice(0,_buttonsArr.length);

function sum2(){
  ctx.fillStyle = "black";
  ctx.fillRect(0,0,w,h)

  ctx.font = "13px Arial"
  ctx.fillStyle = "white";

  ctx.fillText(`Good job!`,w/2,30);
  ctx.fillText(`You were able to guide your people into the mountains, where`,w/2,60);
  ctx.fillText(`it is much harder for the portugeese to get you,`,w/2,90);
  ctx.fillText(`Now lets take a look at one of the many ways`,w/2,120);
  ctx.fillText(`native americans resisted europeans trying to invade their land.`,w/2,150);

  ctx.fillText(`You are now Pontiac, and you have allied with many other native american tribes,`,w/2,200);
  ctx.fillText(`your plan is for every tribe to lead an attack`,w/2,230);
  ctx.fillText(`on the closest european base near them, and then all come thogether and take down the`,w/2,260);
  ctx.fillText(`remaining forts.`,w/2,290);

  
  sum3Button.onclick = () => {
    scene = "sum3()"
    sceneFirstFrame = true;
  }
  sum3Button.onhover = () => {
    sum3Button.w += 10;
    sum3Button.x -= 5;
    sum3Button.h += 10;
    sum3Button.y -= 5;
  }
  sum3Button.onout = () => {
    sum3Button.w -= 10;
    sum3Button.x += 5;
    sum3Button.h -= 10;
    sum3Button.y += 5;
  }
  
  sum3Button.push();
  sum3Button.draw();
}
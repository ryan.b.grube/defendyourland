var lvl2BgImg = new Image();
lvl2BgImg.src = "../imgs/dessert.jpg"

var imgLoaded = false;

lvl2BgImg.onload = () => {imgLoaded = true}

var troopSpawn = 0;
var troops = 0;

var troopint = setInterval(()=>{troopSpawn ++;},1)

var currentTroops = [];
var currentArrows = [];

var arrowsLeft = 200;

arrowGuage = new Guage(10,10,100,20,arrowsLeft/10)

var troopsLeft = 15;

troopGuage = new Guage(120,10,100,20,troopsLeft/15)

class Troop{
  constructor(x,y,speed){

    this.x = x;
    this.y = y;
    this.w = 25;
    this.h = 25;
    this.speed = speed;

    this.draw = () => {
      ctx.fillStyle = "rgba(100,0,0,0.5)"
      ctx.fillRect(this.x,this.y,this.w,this.h)

      currentArrows.forEach(a=>{
        a.x2 = a.x + (20*Math.cos(a.angle))
        a.y2 = a.y + (20*Math.sin(a.angle))
        if(
          (
          a.x > this.x 
          && a.x < this.x + this.w
          && a.y > this.y
          && a.y < this.y + this.h
          ) ||
          (
          a.x2 > this.x 
          && a.x2 < this.x + this.w
          && a.y2 > this.y
          && a.y2 < this.y + this.h
          )
        ){
            if(currentTroops.indexOf(this) != null){
              currentTroops.splice(currentTroops.indexOf(this),1)
              troopsLeft --;
            }
            if(currentArrows.indexOf(a) != null){
              currentArrows.splice(currentArrows.indexOf(a),1)
            }
        }
      })
    }
    this.update = () => {
      this.x -= this.speed;
    }

  }
}
class Arrow{
  constructor(x,y,vector,speed){
    if(typeof vector === 'Object'){
      this.angle = Math.atan2(vector.x,vector.y);
    }else{
      this.angle = vector;
    }
    this.x = x;
    this.y = y;
    this.speed = speed;

    this.draw = () => {
      ctx.beginPath();
      ctx.moveTo(this.x,this.y);
      ctx.lineTo(this.x + (20*Math.cos(this.angle)),this.y + (20*Math.sin(this.angle)))
      ctx.strokeStyle = "white";
      ctx.stroke();
    }

    this.update = () => {
      this.x += this.speed * Math.cos(this.angle)
      this.y += this.speed * Math.sin(this.angle)
    }
  }
}
class Soldier{
  constructor(x,y,angle){

    this.x = x;
    this.y = y;
    this.angle = angle;

    this.draw = () => {
      
      this.angle = Math.atan2(mouseY-this.y,mouseX-this.x)

      ctx.fillStyle = "rgba(0,100,100,0.5)"
      ctx.beginPath();
      ctx.arc(this.x,this.y,5,0,Math.PI * 2);
      ctx.fill()

      ctx.beginPath()
      ctx.strokeStyle = "rgba(0,100,100,0.5)"
      ctx.moveTo(this.x,this.y);
      ctx.lineTo(this.x+(50*Math.cos(this.angle)),this.y+(50*Math.sin(this.angle)))
      ctx.stroke();
    }

  }
}

currentSoldiers = [];
for(i=0;i<20;i++){
  currentSoldiers.push(new Soldier(10,i * (h/20) + 10,0))
}

function level2(){
  _buttonsArr.splice(0,_buttonsArr.length);
  updateLevel2();
  drawLevel2();
}

function updateLevel2(){
  if(troopsLeft <= 0){
    scene="win()"
  }
  currentTroops.forEach(t=>{
    if(t.x<=0){
      scene="loss()"
    }
  })
  if(troopSpawn >= 500){
    troops ++;
    if(troops == 15){
      clearInterval(troopint)
    }
    troopSpawn = 0;
    currentTroops.push(new Troop(700,(Math.random()*380)+20,Math.random()*2))
  }
}

function drawLevel2(){

  //draw background image
  if(imgLoaded){
    ctx.drawImage(lvl1BgImg,0,0,w,h)
  }

  currentSoldiers.forEach(s=>{s.draw()})
  currentTroops.forEach(t=>{t.update();t.draw()})
  currentArrows.forEach(a=>{a.update();a.draw()})

  arrowGuage.val = arrowsLeft/200;
  arrowGuage.draw();
  ctx.font = "12px Arial"
  ctx.fillStyle = "black";
  ctx.fillText("Arrows left: " + arrowsLeft,10+50,22)

  troopGuage.val = troopsLeft/15;
  troopGuage.draw();
  ctx.font = "12px Arial"
  ctx.fillStyle = "black";
  ctx.fillText("Enimies left: " + troopsLeft,120+50,22)
}

document.addEventListener('click',()=>{
  if(scene == "level2()"){
    if(arrowsLeft > 0){
    currentSoldiers.forEach(s=>{
      a = new Arrow(s.x,s.y,s.angle,12)
      arrowsLeft --;
      currentArrows.push(a)
    })
    }
  }
})
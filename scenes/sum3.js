playLevel2Button = new Button("Start",w/2-90,330,180,40,"lightgray","white","black","30px Arial")

_buttonsArr.splice(0,_buttonsArr.length);

function sum3(){
  ctx.fillStyle = "black";
  ctx.fillRect(0,0,w,h)

  ctx.font = "13px Arial"
  ctx.fillStyle = "white";

  ctx.fillText(`You need to take down a european base`,w/2,30);
  ctx.fillText(`command your troops`,w/2,60);
  ctx.fillText(`and destroy the incoming european invaders`,w/2,90);
  ctx.fillText(`click to fire arrows, but be spareing, because you only have a limited supply.`,w/2,120);

  
  playLevel2Button.onclick = () => {
    scene = "level2()"
    sceneFirstFrame = true;
  }
  playLevel2Button.onhover = () => {
    playLevel2Button.w += 10;
    playLevel2Button.x -= 5;
    playLevel2Button.h += 10;
    playLevel2Button.y -= 5;
  }
  playLevel2Button.onout = () => {
    playLevel2Button.w -= 10;
    playLevel2Button.x += 5;
    playLevel2Button.h -= 10;
    playLevel2Button.y += 5;
  }
  
  playLevel2Button.push();
  playLevel2Button.draw();
}
retryButton = new Button("Again!",w/2-90,230,180,40,"lightgray","white","black","30px Arial")
articalButton = new Button("Read about these events",w/2-90,280,180,40,"lightgray","white","black","14px Arial")
sourceButton = new Button("Source code (Made by Ryan Grube)",w/2-90,330,180,40,"lightgray","white","black","11px Arial")

function win(){
  ctx.fillStyle = "black";
  ctx.fillRect(0,0,w,h)

  ctx.font = "13px Arial"
  ctx.fillStyle = "white";

  ctx.fillText(`Congatulations!`,w/2,30);
  ctx.fillText(`you managed to save African and Native american tribes from the slave trade,`,w/2,60);
  ctx.fillText(`Unforunatly, this is not exaxctly how these events played out in real life.`,w/2,90);
  ctx.fillText(`Click the artical button below to read more about this.`,w/2,120);
  
retryButton.onclick = () => {
  scene = "mainMenu()"
}
retryButton.onhover = () => {
  retryButton.w += 10;
  retryButton.x -= 5;
  retryButton.h += 10;
  retryButton.y -= 5;
}
retryButton.onout = () => {
  retryButton.w -= 10;
  retryButton.x += 5;
  retryButton.h -= 10;
  retryButton.y += 5;
}
  
articalButton.onclick = () => {
  scene = "mainMenu()"
}
articalButton.onhover = () => {
  articalButton.w += 10;
  articalButton.x -= 5;
  articalButton.h += 10;
  articalButton.y -= 5;
}
articalButton.onout = () => {
  articalButton.w -= 10;
  articalButton.x += 5;
  articalButton.h -= 10;
  articalButton.y += 5;
}

sourceButton.onclick = () => {
  scene = "window.location = 'https://gitlab.com/ryan.b.grube/defendyourland'"
}
sourceButton.onhover = () => {
  sourceButton.w += 10;
  sourceButton.x -= 5;
  sourceButton.h += 10;
  sourceButton.y -= 5;
}
sourceButton.onout = () => {
  sourceButton.w -= 10;
  sourceButton.x += 5;
  sourceButton.h -= 10;
  sourceButton.y += 5;
}


  retryButton.push();
  retryButton.draw();
  articalButton.push();
  articalButton.draw();
  sourceButton.push();
  sourceButton.draw();
}
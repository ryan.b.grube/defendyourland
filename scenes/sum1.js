playLevel1Button = new Button("Start",w/2-90,330,180,40,"lightgray","white","black","30px Arial")

function sum1(){
  ctx.fillStyle = "black";
  ctx.fillRect(0,0,w,h)

  ctx.font = "13px Arial"
  ctx.fillStyle = "white";

  ctx.fillText(`Your brother has committed scuicide due to the high slave demands from portugal.`,w/2,30);
  ctx.fillText(`You are now the queen of Ndonga, a tribe in africa, and it is your job to defend your village`,w/2,60);
  ctx.fillText(`and stop the portugeese slave traders. Your first plan to hold of the portugeese is to move`,w/2,90);
  ctx.fillText(`your villages to more inaccseible places such as maountains. This will slow down the `,w/2,120);
  ctx.fillText(`portugeese, and give you more time to stratagize, as well as a fighting advantage.`,w/2,150);

  ctx.fillText(`Use the w/a/s/d keys to navigate your forces through the portugeese`,w/2,200);
  ctx.fillText(`basecamps, and to to the mountains (green square)`,w/2,230);
  ctx.fillText(`if you run into a portugeese camp (red boxes) or a wall, you will be captured,`,w/2,260);
  ctx.fillText(`and your people will be traded as slaves.`,w/2,290);

  
  playLevel1Button.onclick = () => {
    scene = "level1()"
    sceneFirstFrame = true;
  }
  playLevel1Button.onhover = () => {
    playLevel1Button.w += 10;
    playLevel1Button.x -= 5;
    playLevel1Button.h += 10;
    playLevel1Button.y -= 5;
  }
  playLevel1Button.onout = () => {
    playLevel1Button.w -= 10;
    playLevel1Button.x += 5;
    playLevel1Button.h -= 10;
    playLevel1Button.y += 5;
  }
  
  playLevel1Button.push();
  playLevel1Button.draw();
}
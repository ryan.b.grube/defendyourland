var lvl1BgImg = new Image();
lvl1BgImg.src = "../imgs/dessert.jpg"

var imgLoaded = false;

lvl1BgImg.onload = () => {imgLoaded = true}

class Basecamp{
  constructor(x,y,w,h){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.draw = () => {
      ctx.strokeStyle ="red";
      ctx.strokeRect(this.x,this.y,this.w,this.h)
    }
  }
}
var camps = [
  new Basecamp(30,30,90,60),
  new Basecamp(105,90,15,30),

  new Basecamp(150,-1,115,90),

  new Basecamp(1,h/2 - 80,65,65),
  new Basecamp(1,h/2 + 15,65,65),

  new Basecamp(105,h/2 - 80,160,160),
  new Basecamp(265,h/2 + 50,30,30),

  new Basecamp(295,0,160,200),

  new Basecamp(330,200,30,170),
  new Basecamp(390,230,30,170),

  new Basecamp(1,h-106,105,105),
  new Basecamp(125,h-106,30,30),

  new Basecamp(190,h-60,130,30),
  new Basecamp(160,h-40,30,10),

  new Basecamp(455,h-100,70,120),
  new Basecamp(535,h-100,70,120),
  new Basecamp(455,h-190,120,50),

  new Basecamp(489,h-270,110,50),

  new Basecamp(489,30,80,60),
]
var guidePoints = [
  {x:10,y:h/2},
  {x:85,y:h/2},
  {x:85,y:h/2 - 95},
  {x:15,y:h/2 - 95},
  {x:15,y:15},
  {x:135,y:15},
  {x:135,y:h/2 - 95},
  {x:280,y:h/2 - 95},
  {x:280,y:h/2 + 25},
  {x:312,y:h/2 + 25},
  {x:312,y:h - 90},
  {x:175,y:h - 90},
  {x:175,y:h - 60},
  {x:135,y:h - 60},
  {x:135,y:h - 10},
  {x:375,y:h - 10},
  {x:375,y:h/2 + 15},
  {x:435,y:h/2 + 15},
  {x:435,y:h/2 + 85},
  {x:590,y:h/2 + 85},
  {x:590,y:h/2 - 5},
  {x:472,y:h/2 - 5},
  {x:472,y:10},
  {x:585,y:10},
  {x:585,y:10},
  {x:585,y:115},
  {x:472,y:115},
]
var queen = {
  x:10,
  y:h/2,
  angle: Math.PI/2,
  speed: 0,
}

function level1(){
  _buttonsArr.splice(0,_buttonsArr.length);
  if(sceneFirstFrame){
    queen = {
      x:10,
      y:h/2,
      angle: Math.PI/2,
      speed: 0,
    }
  }
  updateLevel1();
  drawLevel1();
}

function updateLevel1(){
  queen.y += queen.speed * Math.sin(queen.angle);
  queen.x += queen.speed * Math.cos(queen.angle);
  
  var qx = queen.x;
  var qy = queen.y;

  if(qy < 30 && qx > w-30){
      //goal
      scene = "sum2()"
    }

  camps.forEach(c => {
    if(
      qx + 5 > c.x
    &&qx - 5 < c.x + c.w
    &&qy + 5 > c.y
    &&qy - 5 < c.y + c.h
    ||qx - 5 < 0
    ||qy - 5 < 0
    ||qx + 5 > w
    ||qy + 5 > h
    ){
      //hit wall
      scene = "loss()"
    }
  })
}

function drawLevel1(){

  //draw background image
  if(imgLoaded){
    ctx.drawImage(lvl1BgImg,0,0,w,h)
  }

  //draw player
  ctx.fillStyle = "green";
  ctx.beginPath();
  ctx.arc(queen.x,queen.y,5,0,Math.PI*2)
  ctx.fill();

  //draw camps
  camps.forEach(c => {
    c.draw();
  })

  //draw path
  ctx.strokeStyle = "rgba(0,0,0,0.4)";
  ctx.beginPath();
  guidePoints.forEach((g,i) => {
    ctx.moveTo(g.x,g.y)
    if(typeof guidePoints[i+1] !== 'undefined'){
      ctx.lineTo(guidePoints[i+1].x,guidePoints[i+1].y)
    }
  })
  ctx.setLineDash([5,5]);
  ctx.stroke();
  ctx.setLineDash([]);

  ctx.fillStyle = "rgba(0,255,0,0.5)";
  ctx.fillRect(w-30,0,30,30)
}

document.onkeydown = (e) => {
  queen.speed = 1;
  switch(e.key){
    case "w":
      queen.angle=-Math.PI/2;
    break;
    case "a":
      queen.angle=Math.PI;
    break;
    case "s":
      queen.angle=-(Math.PI*3)/2;
    break;
    case "d":
      queen.angle=Math.PI*2;
    break;
  }
}
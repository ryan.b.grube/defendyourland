resetButton = new Button("Restart",w/2-90,330,180,40,"lightgray","white","black","30px Arial")

function loss(){
  ctx.fillStyle = "black";
  ctx.fillRect(0,0,w,h)

  ctx.font = "13px Arial"
  ctx.fillStyle = "white";

  ctx.fillText(`You have lost`,w/2,30);
  ctx.fillText(`the europeans have taken over your forces,`,w/2,60);
  ctx.fillText(`and are taking your citizens as slaves.`,w/2,90);

  
  resetButton.onclick = () => {
    scene = "mainMenu()"
  }
  resetButton.onhover = () => {
    resetButton.w += 10;
    resetButton.x -= 5;
    resetButton.h += 10;
    resetButton.y -= 5;
  }
  resetButton.onout = () => {
    resetButton.w -= 10;
    resetButton.x += 5;
    resetButton.h -= 10;
    resetButton.y += 5;
  }
  
  resetButton.push();
  resetButton.draw();
}
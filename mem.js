var c = document.getElementById("c");
var ctx = c.getContext('2d')

var w = c.width = 600;
var h = c.height = 400;

var mouseX,mouseY = 0;

sceneFirstFrame = true;
scene = "mainMenu()";
paused = false;

_buttonsArr = [];

function lerp (start, end, amt){
  return (1-amt)*start+amt*end
}

class Button{
  constructor(
    text,
    x,y,
    w,h,
    bgColor = "red",
    strokeColor = "black",
    color = "black",
    font = "12px Arial",
    onclick = () => {},
    onhover = () => {},
    onout = () => {},
    ){
    this.text = text;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.bgColor = bgColor;
    this.strokeColor = strokeColor;
    this.color = color;
    this.font = font;
    this.onclick = onclick;
    this.onhover = onhover;
    this.onout = onout;
    this.in = false;


    this.push = () => {
      if(!_buttonsArr.includes(this)){
        _buttonsArr.push(this)
      }
    }

    this.draw = () => {
      ctx.fillStyle = this.bgColor;
      ctx.fillRect(this.x,this.y,this.w,this.h);
      ctx.strokeStyle = this.strokeColor;
      ctx.strokeRect(this.x,this.y,this.w,this.h);
      ctx.fillStyle = this.color;
      ctx.font = this.font;
      ctx.textAlign = "center";
      ctx.textBaseline = "middle"
      ctx.fillText(this.text,this.x+this.w/2,this.y+this.h/2);
      ctx.textBaseline = "alphabetical"
    }

  }
}
class Guage{
  constructor(
    x,y,w,h,
    val,
    ){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.val = val;


    this.draw = () => {
      ctx.fillStyle = "red";
      ctx.fillRect(this.x,this.y,lerp(0,this.w,this.val),this.h);
      
      ctx.strokeStyle = "black";
      ctx.lineWidth = 5;
      ctx.strokeRect(this.x,this.y,this.w,this.h);
      ctx.lineWidth = 1;
    }

  }
}

document.onclick = (e) => {
  var rect = c.getBoundingClientRect();
  var mouseX = e.clientX - rect.left;
  var mouseY = e.clientY - rect.top;

  _buttonsArr.forEach(b => {
    if(mouseX > b.x && mouseX < (b.x + b.w) && mouseY > b.y && mouseY < (b.y + b.h)){
      b.onclick();
    }
  })
}

document.onmousemove = (e) => {
  var rect = c.getBoundingClientRect();
  mouseX = e.clientX - rect.left;
  mouseY = e.clientY - rect.top;

  _buttonsArr.forEach(b => {
    if(mouseX > b.x && mouseX < (b.x + b.w) && mouseY > b.y && mouseY < (b.y + b.h)){
      if(!b.in){
        b.onhover();
        b.in = true;
      }
    }else{
      if(b.in){
        b.onout();
        b.in = false;
      }
    }
  })

}